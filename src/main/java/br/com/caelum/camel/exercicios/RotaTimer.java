package br.com.caelum.camel.exercicios;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.xstream.XStreamDataFormat;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.thoughtworks.xstream.XStream;

import br.com.caelum.camel.Negociacao;

public class RotaTimer {
	private static final String SQL_INSERIR = "insert into negociacao(preco, quantidade, data) values (${property.preco}, ${property.quantidade}, '${property.data}')";

	public static void main(String[] args) throws Exception {
		DefaultCamelContext context = new DefaultCamelContext(criaDataSource());
		context.setProperties(criaConfiguracaoProxy());
		context.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				final XStream xstream = new XStream();
				xstream.alias("negociacao", Negociacao.class);
				
				from("timer:negociacoes?fixedRate=true&delay=1s&period=1s")
				.to("http4://argentumws-spring.herokuapp.com/negociacoes")
				.unmarshal(new XStreamDataFormat(xstream))
				.split(body())
				.process(new Processor() {
				
					@Override
			        public void process(Exchange exchange) throws Exception {
			            Negociacao negociacao = exchange.getIn().getBody(Negociacao.class);
			            exchange.setProperty("preco", negociacao.getPreco());
			            exchange.setProperty("quantidade", negociacao.getQuantidade());
			            String data = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(negociacao.getData().getTime());
			            exchange.setProperty("data", data);
			        }
				})
				.setBody(simple(SQL_INSERIR))
				.log("${body}")
				.to("jdbc:mysql");
			}
		});
		context.start();
		Thread.sleep(20000);
		context.stop();
	}
	
	private static HashMap<String, String> criaConfiguracaoProxy() {
		HashMap<String, String> properties = new HashMap<String, String>();
		properties.put("http.proxyHost", "");
		properties.put("http.proxyPort", "");
		return properties;
	}
	
	private static SimpleRegistry criaDataSource() throws SQLException {
		SimpleRegistry registro = new SimpleRegistry();
	    MysqlConnectionPoolDataSource mysqlDs = new MysqlConnectionPoolDataSource();
	    mysqlDs.setDatabaseName("camel");
	    mysqlDs.setServerName("localhost");
	    mysqlDs.setPort(3306);
	    mysqlDs.setUser("root");
	    mysqlDs.setPassword("123456");
	    mysqlDs.setServerTimezone("UTC");
	    registro.put("mysql", mysqlDs);
	    return registro;
	}
}
