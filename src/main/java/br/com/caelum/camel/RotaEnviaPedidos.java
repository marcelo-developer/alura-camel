package br.com.caelum.camel;

import static br.com.caelum.camel.Fila.criaComponenteActiveMQ;
import static br.com.caelum.camel.Fila.endpointFila;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext; 

public class RotaEnviaPedidos {

	public static void main(String[] args) throws Exception {
		CamelContext context = new DefaultCamelContext();
		criaComponenteActiveMQ(context);
		context.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				from("file:pedidos?noop=true")
				.to(endpointFila("pedidos"));
			}
		});
		
		context.start();
		Thread.sleep(2000);
		context.stop();
	}
}
