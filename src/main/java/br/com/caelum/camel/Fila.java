package br.com.caelum.camel;

import java.time.LocalDateTime;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.camel.CamelContext;

public class Fila {
	public static final String ACTIVEMQ_COMPONENT = "activemq";
	
	public static void criaComponenteActiveMQ(CamelContext context) {
		ActiveMQComponent componente = ActiveMQComponent.activeMQComponent("tcp://localhost:61616");
		componente.setClientId(LocalDateTime.now().toString());
		context.addComponent(ACTIVEMQ_COMPONENT, componente);
	}
	
	public static String endpointFila(String nome) {
		return ACTIVEMQ_COMPONENT + ":queue:" + nome;
	}
	
	public static String endpointDLQ(String nome) {
		return endpointFila(nome) + ".DLQ";
	}
}
