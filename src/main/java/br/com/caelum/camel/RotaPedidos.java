package br.com.caelum.camel;


import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.impl.DefaultCamelContext;

import static br.com.caelum.camel.Fila.criaComponenteActiveMQ; 
import static br.com.caelum.camel.Fila.endpointFila; 
import static br.com.caelum.camel.Fila.endpointDLQ; 

public class RotaPedidos {

	public static void main(String[] args) throws Exception {
		CamelContext context = new DefaultCamelContext();
		criaComponenteActiveMQ(context);
		context.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				
//				onException(SAXParseException.class)
//			    	.handled(false)
//			    	.logExhaustedMessageHistory(false)
//			    	.maximumRedeliveries(3)
//			            .redeliveryDelay(4000)
//			        .onRedelivery(errorProcessor());
				
				errorHandler(deadLetterChannel(endpointDLQ("pedidos"))
						.logExhaustedMessageHistory(false)
						.maximumRedeliveries(3)
							.redeliveryDelay(3000)
							.onRedelivery(errorProcessor()));
				
				from(endpointFila("pedidos"))
					.routeId("rota-pedidos")
					.log("${body}")
					.to("validator:pedido.xsd")
					.multicast()
						.to("direct:soap")
						.to("direct:http");
				
				from("direct:http")
					.routeId("rota-http")
					.setProperty("clienteId", xpath("/pedido/pagamento/email-titular/text()"))
					.setProperty("pedidoId", xpath("/pedido/id/text()"))
					.split()
						.xpath("/pedido/itens/item")
					.filter()
						.xpath("/item/formato[text()='EBOOK']")
					.setProperty("ebookId", xpath("/item/livro/codigo/text()"))
					.log("${id}")
					.marshal().xmljson()
//					.log("${body}")
					.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
					.setHeader(Exchange.HTTP_QUERY, simple("ebookId=${exchangeProperty.ebookId}&pedidoId=${property.pedidoId}&clienteId=${property.clienteId}"))
				.to("http4://localhost/webservices/ebook/item");
				
				from("direct:soap")
					.routeId("rota-soap")
					.to("xslt:pedido-para-soap.xslt")
//					.log("${body}")
					.setHeader(Exchange.CONTENT_TYPE, constant("text/xml"))
					.to("http4://localhost/webservices/financeiro");
			}

			private Processor errorProcessor() {
				return new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {
						System.out.println(String.format("Reentrega %d de %d.", 
								exchange.getIn().getHeader(Exchange.REDELIVERY_COUNTER),
								exchange.getIn().getHeader(Exchange.REDELIVERY_MAX_COUNTER)));
					}
				};
			}
		});
		
		context.start();
		Thread.sleep(20000);
		context.stop();
	}
}
